import psycopg2

class MyConnectionDB():
    def __init__(self, db="pgpython", user="postgres", passW="832862ng@", hosts="127.0.0.1", ports="5432"):
        self.conn = psycopg2.connect(database=db, user=user, password=passW, host=hosts, port=ports)
        self.cur = self.conn.cursor()
    
    def query(self, query):
        self.cur.execute(query)
    
    def close(self):
        self.cur.close()
        self.conn.close()